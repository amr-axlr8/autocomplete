﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Web.Http.Cors;

namespace autocomplete.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SearchController : ApiController
    {
        [HttpGet]
        public List<Customer> searchCustomer(int index = 1, int size = 10, string name = "")
        {
            if (size < 1) size = 200;
            if (index < 1) index = 1;
            using (masterEntities context = new masterEntities())
            {
                return context.Customers
                                        .Where(c => c.FirstName.StartsWith(name))
                                        .OrderBy(c => c.FirstName)
                                        .ToPagedList(index, size)
                                        .ToList();
            }
        }
        [HttpPost]
        public object searchCustomer(SearchOpj searchOpj)
        {
            try
            {

                if (searchOpj.pageSize < 1 || searchOpj.pageSize > 1000)
                {
                    // throw page size exeption
                    searchOpj.pageSize = 1000;
                }
                if (searchOpj.pageIndex < 1)
                {
                    // throw page index exeption
                    searchOpj.pageIndex = 1;
                }
                if (searchOpj.orderBy == null)
                {
                    searchOpj.orderBy = CustomerProperty.FirstName;
                }
                
                if (searchOpj.orderType == null)
                {
                    searchOpj.orderType = OrderTypes.ASC;
                }

               
               

                using (masterEntities context = new masterEntities())
                {
                    var customers = context.Customers.AsQueryable();
                    // siwtch over cusomer properties 
                    if(searchOpj.customer != null)
                    {

                            // id filter
                            if (searchOpj.customer.Id != null && searchOpj.customer.Id != 0)
                            {
                                customers = customers.Where(c => c.Id.Equals(searchOpj.customer.Id));
                            }

                            // firstname filter
                            if (!String.IsNullOrEmpty(searchOpj.customer.FirstName))
                            {
                                customers = customers.Where(c => c.FirstName.Contains(searchOpj.customer.FirstName));
                            }

                            // LastName filter
                            if (!String.IsNullOrEmpty(searchOpj.customer.LastName))
                            {
                                customers = customers.Where(c => c.LastName.Contains(searchOpj.customer.LastName));
                            }

                            // Country filter
                            if (!String.IsNullOrEmpty(searchOpj.customer.Country))
                            {
                                customers = customers.Where(c => c.Country.Contains(searchOpj.customer.Country));
                            }

                            // City filter
                            if (!String.IsNullOrEmpty(searchOpj.customer.City))
                            {
                                customers = customers.Where(c => c.City.Contains(searchOpj.customer.City));
                            }

                            // Phone filter
                            if (!String.IsNullOrEmpty(searchOpj.customer.Phone))
                            {
                                customers = customers.Where(c => c.Phone.Contains(searchOpj.customer.Phone));
                            }
                    }




                    // orderby
                    switch (searchOpj.orderType)
                    {

                        case OrderTypes.ASC:
                            {
                                customers = customers.OrderBy(searchOpj.orderBy+"");
                                break;
                            }
                        case OrderTypes.DES:
                            {
                                customers = customers.OrderBy(searchOpj.orderBy +" descending");
                                break;
                            }
                    }
                    
                    // get count
                    int customersCount = customers.Count();
                    // makePaging

                    List<Customer> customerList = (List<Customer>)customers.ToPagedList(searchOpj.pageIndex, searchOpj.pageSize).ToList();


                    return  new { count  = customersCount, data = customerList };

                }
            }
            catch (Exception x)
            {
                return x;
            }
          
        }
    }
    /*
     int queryTotalCount = context.Customers
                                           .Where(c => c.Id.Equals(searchOpj.query))
                                           .OrderBy(c => c.FirstName)
                                           .Count();

                                List<Customer> customerList = (List < Customer >) context.Customers
                                                        .Where(c => c.FirstName.StartsWith(searchOpj.query))
                                                        .OrderBy(c => c.FirstName)
                                                        .ToList()
                                                        .ToPagedList(searchOpj.pageIndex, searchOpj.pageSize);*/
    public class SearchOpj
    {
        //public string query { set; get; }
        //public CustomerProperty searchFeild { set; get; }
        public Customer customer { set; get; }
        public int pageIndex { set; get; }
        public int pageSize { set; get; }
        public CustomerProperty orderBy { set; get; }
        public OrderTypes orderType { set; get; }
    }


    public enum CustomerProperty
    {
        Id ,
        FirstName,
        LastName ,
        City,
        Country,
        Phone
    }

    public enum OrderTypes
    {
        ASC ,
        DES
    }


}
